// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "noff.h"
//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	First, set up the translation from program memory to physical 
//	memory.  For now, this is really simple (1:1), since we are
//	only uniprogramming, and we have a single unsegmented page table
//
//	"executable" is the file containing the object code to load into memory
//----------------------------------------------------------------------
AddrSpace::AddrSpace(){

}

AddrSpace::AddrSpace(OpenFile *executable)
{
    NoffHeader noffH;
    unsigned int i, size;
//    unsigned vpn, offset;
//    TranslationEntry *entry;
//    unsigned int pageFrame;
    numSharedPages = 0;


    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);

// how big is address space?
    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size 
			+ UserStackSize;	// we need to increase the size
						// to leave room for the stack
    numPages = divRoundUp(size, PageSize);
    size = numPages * PageSize;
DEBUG('N',"Code -- addr:%d, size:%d -- Data -- addr:%d, size:%d , totalSize:%d, totalPages:%d\n",noffH.code.virtualAddr,noffH.code.size,noffH.initData.virtualAddr,noffH.initData.size,size,numPages );
//    ASSERT(numPages+numPagesAllocated <= NumPhysPages);		// check we're not trying
										// to run anything too big --
										// at least until we have
										// virtual memory

    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPages, size);
// first, set up the translation 
    pageTable = new TranslationEntry[numPages];
    for (i = 0; i < numPages; i++) {
	pageTable[i].virtualPage = i;
//    pageTable[i].physicalPage = i+numPagesAllocated;
    pageTable[i].physicalPage = -1;
	pageTable[i].valid = FALSE;
	pageTable[i].use = FALSE;
	pageTable[i].dirty = FALSE;
    pageTable[i].inFile = TRUE;
    pageTable[i].isShared = FALSE;
	pageTable[i].readOnly = FALSE;  // if the code segment was entirely on 
					// a separate page, we could set its 
					// pages to be read-only
    }
    currentThread->backupArray = new char[size];
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace (AddrSpace*) is called by a forked thread.
//      We need to duplicate the address space of the parent.
//----------------------------------------------------------------------

AddrSpace::AddrSpace(AddrSpace *parentSpace,int childPid)
{
    DEBUG('F',"Forking thread :%d by pid:%d\n",childPid,currentThread->GetPID());
    numPages = parentSpace->GetNumPages();
    numSharedPages = parentSpace->GetNumSharedPages();
    int vpnAddr = -1;
    unsigned i,j, size = numPages * PageSize;
    ASSERT(threadArray[childPid] != NULL);
    threadArray[childPid]->CopyPageParameters(currentThread);

    DEBUG('N', "Initializing address space, num pages %d, size %d\n",
                                        numPages, size);
    // first, set up the translation
    TranslationEntry* parentPageTable = parentSpace->GetPageTable();
    unsigned int numTotalPages = numPages + numSharedPages;
    pageTable = new TranslationEntry[numTotalPages];
    threadArray[childPid]->space->SetPrivateVar(pageTable,numPages,numSharedPages);
    for (i = 0; i < numTotalPages; i++) {
        pageTable[i].virtualPage = i;
	   pageTable[i].physicalPage = -1;
     DEBUG('N',"At Start -- iteration:%d page for child:%d is :%d, for parent:%d is :%d isShared:%d valid:%d\n",i,childPid,pageTable[i].physicalPage,currentThread->GetPID(),parentPageTable[i].physicalPage,parentPageTable[i].isShared,parentPageTable[i].valid);
        pageTable[i].use = parentPageTable[i].use;
        pageTable[i].dirty = parentPageTable[i].dirty;
        pageTable[i].isShared = parentPageTable[i].isShared;
        pageTable[i].readOnly = parentPageTable[i].readOnly; 
        pageTable[i].valid = parentPageTable[i].valid;
        if(pageTable[i].isShared){
            pageTable[i].physicalPage = parentPageTable[i].physicalPage;
        }
        else if(pageTable[i].valid){
            IntStatus oldLevel = interrupt->SetLevel(IntOff);  
            pageTable[i].physicalPage = FindEmptyPage(i,parentPageTable[i].physicalPage,childPid,&vpnAddr);
            (void) interrupt->SetLevel(oldLevel);
            unsigned startAddrParent = parentPageTable[i].physicalPage*PageSize;
            unsigned startAddrChild = pageTable[i].physicalPage*PageSize;
    DEBUG('N',"copying from startAddrParent:%d  pid:%d to startAddrChild:%d pid:%d\n",startAddrParent,currentThread->GetPID(),startAddrChild,childPid);
            for(j=0;j<PageSize;j++){
                machine->mainMemory[startAddrChild+j] = machine->mainMemory[startAddrParent+j];
            }
            /*if(vpnAddr != -1 && vpnAddr < i){ //this vpnAddr was replaced in FindPageToReplace
                pageTable[vpnAddr].valid = 0;
                //pageTable[vpnAddr].inFile = 0;
            }*/
	    currentThread->SortedInsertInWaitQueue(1000+stats->totalTicks);
        }
        else {} 
     DEBUG('N',"At End -- iteration:%d page for child:%d is :%d, for parent:%d is :%d\n",i,childPid,pageTable[i].physicalPage,currentThread->GetPID(),parentPageTable[i].physicalPage);
    }
DEBUG('N',"Initialized space for childPid:%d with parent:%d",childPid,currentThread->GetPID());
    DEBUG('F',"Forked thread :%d by pid:%d\n",childPid,currentThread->GetPID());
    for(i=0;i<numTotalPages;i++)
        pageTable[i].inFile = parentPageTable[i].inFile;        
    for(i=0;i<size;i++){
        DEBUG('F',"%x ",currentThread->backupArray[i]);
        threadArray[childPid]->backupArray[i] = currentThread->backupArray[i];
      }
      DEBUG('F',"\n");

}

//----------------------------------------------------------------------
// AddrSpace::~AddrSpace
// 	Dealloate an address space.  Nothing for now!
//----------------------------------------------------------------------

AddrSpace::~AddrSpace()
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);   // disable interrupts
   unsigned int i; 
   int deletedPage;
   unsigned int numTotalPages = numPages + numSharedPages;
   for(i = 0;i<numTotalPages;i++){
        if(!pageTable[i].isShared && pageTable[i].valid){
            deletedPage = pageTable[i].physicalPage;
            DeletePageEntry(deletedPage);
            globalPageTable[deletedPage].isAllocated = FALSE;
            globalPageTable[deletedPage].isShared = FALSE;
            numPagesAllocated--;
        }
   }
   delete pageTable;
    (void) interrupt->SetLevel(oldLevel);   // re-enable interrupts
}

void
AddrSpace::SetPrivateVar(TranslationEntry* inputPageTable, int InputNumPages,int InputNumSharedPages){
    pageTable = inputPageTable;
    numPages = InputNumPages;
    numSharedPages = InputNumSharedPages;
}

//----------------------------------------------------------------------
// AddrSpace::InitRegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------


// Written by nishant, used to allocate a shared table
unsigned
AddrSpace::allocateSharedTable(unsigned int len){
    int dummy;
    unsigned freePage,i;
    unsigned int numTotalPages = numPages + numSharedPages;
    unsigned int newNumPages = numTotalPages + len;
    //ASSERT(newNumPages+numPagesAllocated <= NumPhysPages);
    TranslationEntry *newPageTable = new TranslationEntry[newNumPages];
    for (i = 0; i < numTotalPages ; i++) {
        newPageTable[i].virtualPage = i;
        newPageTable[i].physicalPage = pageTable[i].physicalPage;
        newPageTable[i].valid = pageTable[i].valid;
        newPageTable[i].use = pageTable[i].use;
        newPageTable[i].isShared = pageTable[i].isShared;
        newPageTable[i].inFile = pageTable[i].inFile;
        newPageTable[i].dirty = pageTable[i].dirty;
        newPageTable[i].readOnly = pageTable[i].readOnly;    // if the code segment was entirely on
                                                   // a separate page, we could set its
                                                    // pages to be read-only
    }
    for(i = numTotalPages;i<newNumPages;i++){
        newPageTable[i].virtualPage = i;
        IntStatus oldLevel = interrupt->SetLevel(IntOff);  
        freePage = FindEmptyPage(-1,-1,currentThread->GetPID(),&dummy);
        (void) interrupt->SetLevel(oldLevel);
        bzero(&machine->mainMemory[freePage*PageSize],PageSize);
        newPageTable[i].physicalPage = freePage;
        newPageTable[i].valid = TRUE;
        newPageTable[i].use = FALSE;
        newPageTable[i].dirty = FALSE;
        newPageTable[i].readOnly = FALSE;  
        newPageTable[i].isShared = TRUE;
        newPageTable[i].inFile = FALSE;      
    }
    numSharedPages+=len;
    delete pageTable;
    pageTable = newPageTable;
    machine->pageTable = pageTable;
    machine->pageTableSize = numPages+numSharedPages;
    DEBUG('N',"Returning :%d as shared address\n",(pageTable[numTotalPages].virtualPage)*PageSize);
    return (unsigned)( (pageTable[numTotalPages].virtualPage)*PageSize);
}

void
AddrSpace::InitRegisters()
{
    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, 0);	

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, numPages * PageSize - 16);
    DEBUG('a', "Initializing stack register to %d\n", numPages * PageSize - 16);
}

//----------------------------------------------------------------------
// AddrSpace::SaveState
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void AddrSpace::SaveState() 
{}

//----------------------------------------------------------------------
// AddrSpace::RestoreState
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void AddrSpace::RestoreState() 
{
    machine->pageTable = pageTable;
    machine->pageTableSize = numPages+numSharedPages;
}

unsigned
AddrSpace::GetNumPages()
{
   return numPages;
}

TranslationEntry*
AddrSpace::GetPageTable()
{
   return pageTable;
}

void
AddrSpace::readVirtAddr(unsigned int virtAddr, int freePage){
    NoffHeader noffH;
    int pid = currentThread->GetPID();
    char *fileName = &(ThreadNameArray[pid][0]);
    ASSERT(fileName != NULL);
DEBUG('N',"[pid: %d] Opening File %s\n",pid,fileName );
    OpenFile *executable = fileSystem->Open(fileName);
    ASSERT(executable!=NULL);
    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
        (WordToHost(noffH.noffMagic) == NOFFMAGIC))
        SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);
    unsigned size,numPages;
    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size 
			+ UserStackSize;
    numPages = divRoundUp(size, PageSize);
    size = numPages * PageSize;
//---------------------------------------------------
    char* fileArray = new char [size];
    bzero(fileArray,size);
    if (noffH.code.size > 0) {
        DEBUG('a', "Initializing code segment, at 0x%x, size %d\n", 
			noffH.code.virtualAddr, noffH.code.size);
        executable->ReadAt(&(fileArray[noffH.code.virtualAddr]),
			noffH.code.size, noffH.code.inFileAddr);
    }
    if (noffH.initData.size > 0) {
        DEBUG('a', "Initializing data segment, at 0x%x, size %d\n", 
			noffH.initData.virtualAddr, noffH.initData.size);
        executable->ReadAt(&(fileArray[noffH.initData.virtualAddr]),
			noffH.initData.size, noffH.initData.inFileAddr);
    }
    unsigned i;
    unsigned startAddrThread = (int)(virtAddr/PageSize) * PageSize;
    unsigned startAddrMachine = freePage*PageSize;
    for( i=0;i<PageSize;i++)
    	machine->mainMemory[startAddrMachine+i] = fileArray[startAddrThread + i];
    delete [] fileArray;

//-------------------------------------------------- 

 /* This method also works Fine, But I thought it would be nice to simulate the hard disk by reading contigously

// zero out the entire address space, to zero the unitialized data segment 
// and the stack segment
    bzero(&machine->mainMemory[freePage*PageSize], PageSize);
    int codeData = 0,dataCode = 0;
    unsigned int length =-1;
    unsigned int startAddr =-1;
    unsigned int vpn = virtAddr/PageSize;
    unsigned int codeEndAddr = noffH.code.virtualAddr+noffH.code.size;
    unsigned int dataEndAddr = noffH.initData.virtualAddr+noffH.initData.size;
    if(noffH.code.size > 0 && virtAddr >= noffH.code.virtualAddr && virtAddr < noffH.code.virtualAddr+noffH.code.size ){
        codeData = 1;
        if(vpn*PageSize <= noffH.code.virtualAddr){
            startAddr = noffH.code.virtualAddr;
        }
        else{
            startAddr = vpn*PageSize;
        }
        if((vpn+1)*PageSize>codeEndAddr)
            length = codeEndAddr - startAddr;
        else
            length = (vpn+1)*PageSize - startAddr;
        DEBUG('N',"1 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
        executable->ReadAt(&(machine->mainMemory[freePage*PageSize + startAddr%PageSize]),
            length, noffH.code.inFileAddr + startAddr - noffH.code.virtualAddr );        
        // first code then data
        if( noffH.initData.virtualAddr >= codeEndAddr && noffH.initData.virtualAddr<=(vpn+1)*PageSize){
            if((vpn+1)*PageSize < dataEndAddr)
                length = (vpn+1)*PageSize - noffH.initData.virtualAddr;
            else 
                length = noffH.initData.size;
           DEBUG('N',"2 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize + noffH.initData.virtualAddr - vpn*PageSize]),
            length, noffH.initData.inFileAddr);
        }
        // first data then code

        if( dataEndAddr <= noffH.code.virtualAddr && dataEndAddr>vpn*PageSize){
            if(vpn*PageSize >noffH.initData.virtualAddr)
                startAddr = vpn*PageSize;
            else
                startAddr = noffH.initData.virtualAddr;
            DEBUG('N',"3 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,dataEndAddr-startAddr,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize + startAddr%PageSize]),
            dataEndAddr-startAddr, noffH.initData.inFileAddr + startAddr - noffH.initData.virtualAddr ); 
        }
    }

    if(noffH.initData.size > 0 && virtAddr >= noffH.initData.virtualAddr && virtAddr < noffH.initData.virtualAddr+noffH.initData.size ){
        dataCode = 1;
        if(vpn*PageSize <= noffH.initData.virtualAddr){
            startAddr = noffH.initData.virtualAddr;
        }
        else{
            startAddr = vpn*PageSize;
        }
        if((vpn+1)*PageSize>dataEndAddr)
            length = dataEndAddr - startAddr;
        else
            length = (vpn+1)*PageSize - startAddr;
        DEBUG('N',"4 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
        executable->ReadAt(&(machine->mainMemory[freePage*PageSize + startAddr%PageSize]),
            length, noffH.initData.inFileAddr + startAddr - noffH.initData.virtualAddr ); 

        //first code then data
        if(codeEndAddr <= noffH.initData.virtualAddr && codeEndAddr>vpn*PageSize){
            if(vpn*PageSize >noffH.code.virtualAddr)
                startAddr = vpn*PageSize;
            else
                startAddr = noffH.code.virtualAddr;
           DEBUG('N',"5 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,codeEndAddr-startAddr,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize + startAddr%PageSize]),
            codeEndAddr-startAddr, noffH.code.inFileAddr + startAddr - noffH.code.virtualAddr ); 
        }


        // first data then code
        if( noffH.code.virtualAddr >= dataEndAddr && noffH.code.virtualAddr<=(vpn+1)*PageSize){
            if((vpn+1)*PageSize < codeEndAddr)
                length = (vpn+1)*PageSize - noffH.code.virtualAddr;
            else 
                length = noffH.code.size;
         DEBUG('N',"6 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize + noffH.code.virtualAddr - vpn*PageSize]),
            length, noffH.code.inFileAddr);
        }
    
    }

    if(!dataCode && !codeData){
        if(noffH.code.size > 0 && codeEndAddr > vpn*PageSize){
            length = codeEndAddr - vpn*PageSize;
            startAddr = vpn*PageSize;
           DEBUG('N',"7 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize]),
            codeEndAddr - vpn*PageSize, noffH.code.inFileAddr  + noffH.code.size - length);            
        }
        if(noffH.initData.size > 0 && dataEndAddr > vpn*PageSize){
            length = dataEndAddr - vpn*PageSize;
            startAddr = vpn*PageSize;
        DEBUG('N',"8 -- virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
            executable->ReadAt(&(machine->mainMemory[freePage*PageSize]),
            dataEndAddr - vpn*PageSize, noffH.initData.inFileAddr + noffH.initData.size - length );
        }
    }
DEBUG('N',"At last --virtAddr:%d startAddr:%d  length:%d freePage:%d\n",virtAddr,startAddr,length,freePage );
*/
//------------------------------------------------------
    delete executable;
    return;
}

void
printQueue(int head,int tail){
    int i =0;
    if(head == -1)
        printf("Queue is Empty\n");
    else{
        while(head!=-1 && i <64){
            printf("curr:%d nextIndex:%d prevIndex:%d tail:%d\n",head,globalPageTable[head].nextIndex,globalPageTable[head].prevIndex,tail );
            head = globalPageTable[head].nextIndex;
            i++;
        }
    }
    printf("Printed Queue\n");
}

void
printLRU_CLOCK(){
    printf("LRUhand is:%d\n",LRUhand);
    int i;
    for(i=0;i<NumPhysPages;i++){
        printf("page:%d refBit:%d \n",i,globalPageTable[i].refBit );
    }
}

unsigned int
AddrSpace::FindPageToReplace(int parentPage,int *vpnAddr){
        /* In this function, set isReplaced option, 
        ,, copy the pageTable in the BACKUP array of thread
        update the corresponding thread's vpn inFile field
        */
//    IntStatus oldLevel = interrupt->SetLevel(IntOff);  
        int freePage;
        if(pageReplacementAlgo == RANDOM){
            freePage  = (int)(Random()%NumPhysPages);
            while(globalPageTable[freePage].isShared || freePage == parentPage ){
                freePage  = (int)(Random()%NumPhysPages);
            }
        }
        else if(pageReplacementAlgo == FIFO){
            ASSERT(FifoHead != -1);
            freePage = FifoHead;
            if(freePage == parentPage){
                freePage = globalPageTable[FifoHead].nextIndex;
                if(globalPageTable[freePage].nextIndex != -1){
                    int Next = globalPageTable[freePage].nextIndex;
                    int Prev = globalPageTable[freePage].prevIndex;
                    globalPageTable[Next].prevIndex = Prev;
                    globalPageTable[Prev].nextIndex = Next;
                    globalPageTable[FifoTail].nextIndex = freePage;
                    globalPageTable[freePage].prevIndex =  FifoTail;
                    FifoTail = freePage;
                }
                else{}
            }
            else if(FifoHead != FifoTail){
                FifoHead = globalPageTable[freePage].nextIndex;
                globalPageTable[FifoTail].nextIndex = freePage;
                globalPageTable[freePage].prevIndex =  FifoTail;
                FifoTail = freePage;
            }
            else {}
                //-------------------------
            globalPageTable[FifoTail].nextIndex = -1;
            globalPageTable[FifoHead].prevIndex = -1;
              if (DebugIsEnabled('P'))
                    printQueue(FifoHead,FifoTail);         
        }
        else if(pageReplacementAlgo == LRU){
            // moving nextIndex moves to MruHead
            ASSERT(LruHead != -1);
            if(LruHead == parentPage){
                freePage = globalPageTable[parentPage].nextIndex;
                if(MruHead != freePage){
                    globalPageTable[MruHead].nextIndex = parentPage;
                    globalPageTable[parentPage].prevIndex = MruHead;
                    LruHead = globalPageTable[freePage].nextIndex;
                    MruHead = freePage;
                }
            }
            else{
                freePage = LruHead;
                LruHead = globalPageTable[freePage].nextIndex;
                globalPageTable[MruHead].nextIndex = freePage;
                globalPageTable[freePage].prevIndex = MruHead;
                MruHead = freePage;
            }
            globalPageTable[MruHead].nextIndex = -1;
            globalPageTable[LruHead].prevIndex = -1;
        }
        else if(pageReplacementAlgo == LRU_CLOCK){
        //    LRUhand = (LRUhand+1)%NumPhysPages;
            while(globalPageTable[LRUhand].refBit || globalPageTable[LRUhand].isShared || LRUhand == parentPage ){
                globalPageTable[LRUhand].refBit = 0;
                LRUhand = (LRUhand+1)%NumPhysPages;
            }
            freePage = LRUhand;
            globalPageTable[freePage].refBit = 1;
            if(parentPage!=-1)
                globalPageTable[parentPage].refBit = 1;
        }
        TranslationEntry *threadPageTable;
        ASSERT(!globalPageTable[freePage].isShared);
        ASSERT(freePage!=parentPage);
        int pidpid = globalPageTable[freePage].thread->GetPID();
        
        if(globalPageTable[freePage].thread->space == NULL){
            *vpnAddr = globalPageTable[freePage].virtualIndex;
DEBUG('P',"parentPage is:%d--Replacing page:%d, holded by thread:%d at position:%d whose pageTable is non-existant\n",parentPage,freePage,pidpid,globalPageTable[freePage].virtualIndex );
        ASSERT(0);
        }
        else{
            threadPageTable = globalPageTable[freePage].thread->space->GetPageTable();
            threadPageTable[globalPageTable[freePage].virtualIndex].valid = 0;
            if(threadPageTable[globalPageTable[freePage].virtualIndex].dirty){
                threadPageTable[globalPageTable[freePage].virtualIndex].inFile = 0;
                unsigned i;
                unsigned startAddrMachine = freePage*PageSize;
                unsigned startAddrThread = (globalPageTable[freePage].virtualIndex)*PageSize;
                ASSERT(globalPageTable[freePage].thread->backupArray != NULL);
                for(i=0;i<PageSize;i++){
                    DEBUG('M',"%d:i Machine:%d thread:%d\n",i,globalPageTable[freePage].thread->backupArray[i+startAddrThread],machine->mainMemory[i+startAddrMachine]);
                   globalPageTable[freePage].thread->backupArray[i+startAddrThread] = machine->mainMemory[i+startAddrMachine];
                }
                DEBUG('N',"Copied from startAddrMachine:%d to startAddrThread:%d pid:%d\n",startAddrMachine,startAddrThread ,pidpid);
            }
        int abc = globalPageTable[freePage].virtualIndex;
DEBUG('P',"parentPage is:%d--Replacing page:%d, holded by thread:%d at position:%d whose valid bit is:%d\n",parentPage,freePage,pidpid,abc,threadPageTable[abc].valid );
        }
 //   (void) interrupt->SetLevel(oldLevel);
    return (unsigned int) freePage;
}

unsigned int
AddrSpace::FindEmptyPage(int vpn, int parentPage,int pid, int *vpnAddr){
//    IntStatus oldLevel = interrupt->SetLevel(IntOff);  
    stats->numPageFaults++;   
    unsigned int i = 0;
    int freePage = NumPhysPages;
    /*
    if(numPagesAllocated == NumPhysPages){
        DEBUG('N',"calling Find Page to Replace for vpn:%d and pid:%d\n",vpn,pid);
	freePage = FindPageToReplace(parentPage,vpnAddr);
        if(vpn<0){      //allocating a shared page
            DeletePageEntry(freePage);
            globalPageTable[freePage].isShared = TRUE;
            globalPageTable[freePage].isAllocated = TRUE;
        }
        numPagesAllocated--;

    }
    */
//    else{
    //    if(vpn && pageTable[vpn-1].valid && (pageTable[vpn-1].physicalPage != NumPhysPages-1) && !(globalPageTable[pageTable[vpn-1].physicalPage+1].isAllocated) )
    //        freePage = pageTable[vpn-1].physicalPage+1;
    //    else if(freePage == NumPhysPages){
            i = 0;
            while(i<NumPhysPages){
                if(!globalPageTable[i].isAllocated){
                    freePage = i;
                    if(vpn>=0){  //that is I am not allocating a shared page 
                        PageIsAccessed(freePage);
                    }
                    break;
                }
                i++;
            }
            if(i==NumPhysPages){
                DEBUG('N',"calling Find Page to Replace for vpn:%d and pid:%d\n",vpn,pid);
                freePage = FindPageToReplace(parentPage,vpnAddr);
                if(vpn<0){      //allocating a shared page
                    DeletePageEntry(freePage);
                    globalPageTable[freePage].isShared = TRUE;
                    globalPageTable[freePage].isAllocated = TRUE;
                }
                numPagesAllocated--;
            }
//    }
    numPagesAllocated++;
    if(vpn>=0){
        globalPageTable[freePage].thread = threadArray[pid];
        globalPageTable[freePage].isShared = FALSE;
    }
    else{
        globalPageTable[freePage].isShared = TRUE;
        //numPagesAllocated--;
    }
    globalPageTable[freePage].virtualIndex = vpn;
    globalPageTable[freePage].isAllocated = TRUE;
    DEBUG('N',"$Found %d as freePage for PID:%d$\n",freePage,pid );
//    (void) interrupt->SetLevel(oldLevel);
    return (unsigned int)freePage;
}

void
AddrSpace::PageIsAccessed(int freePage){
//    IntStatus oldLevel = interrupt->SetLevel(IntOff); 
    DEBUG('P',"numPagesAllocated:%d Accessing page:%d pid:%d\n",numPagesAllocated,freePage ,currentThread->GetPID() );
        if(pageReplacementAlgo == FIFO && !globalPageTable[freePage].isAllocated){
            if(FifoHead == -1){
                FifoHead = FifoTail = freePage;
            }
            else if(freePage == FifoHead && freePage != FifoTail){
                globalPageTable[FifoTail].nextIndex = freePage;
                globalPageTable[freePage].prevIndex = FifoTail;
                FifoTail = freePage;
                FifoHead = globalPageTable[freePage].nextIndex;
            }
            else if(freePage != FifoHead && freePage != FifoTail){
                /*
                if(globalPageTable[freePage].isAllocated){
                    int Next = globalPageTable[freePage].nextIndex;
                    int Prev = globalPageTable[freePage].prevIndex;
                    globalPageTable[Next].prevIndex = Prev;
                    globalPageTable[Prev].nextIndex = Next;
                }*/
                globalPageTable[FifoTail].nextIndex = freePage;
                globalPageTable[freePage].prevIndex = FifoTail;
                FifoTail = freePage;               
            }
            else if(freePage == FifoHead && freePage == FifoTail){}
            else if(freePage == FifoTail && freePage != FifoHead) {}
            globalPageTable[FifoTail].nextIndex = -1;
            globalPageTable[FifoHead].prevIndex = -1;
    //printf("FifoHead:%d FifoTail:%d\n",FifoHead,FifoTail );
           if (DebugIsEnabled('P'))
             printQueue(FifoHead,FifoTail);
        }
        else if(pageReplacementAlgo == LRU){
            if(LruHead == -1){
                LruHead = MruHead = freePage;
            }
            else if(freePage == LruHead && freePage != MruHead){
                globalPageTable[MruHead].nextIndex = freePage;
                globalPageTable[freePage].prevIndex = MruHead;
                MruHead = freePage;
                LruHead = globalPageTable[freePage].nextIndex;
            }
            else if(freePage != LruHead && freePage != MruHead){
                if(globalPageTable[freePage].isAllocated){
                    int Next = globalPageTable[freePage].nextIndex;
                    int Prev = globalPageTable[freePage].prevIndex;
                    globalPageTable[Next].prevIndex = Prev;
                    globalPageTable[Prev].nextIndex = Next;
                }
                globalPageTable[MruHead].nextIndex = freePage;
                globalPageTable[freePage].prevIndex = MruHead;
                MruHead = freePage;               
            }
            else if(freePage == LruHead && freePage == MruHead){}
            else if(freePage == MruHead && freePage != LruHead) {}
            globalPageTable[MruHead].nextIndex = -1;
            globalPageTable[LruHead].prevIndex = -1;
    //printf("FifoHead:%d FifoTail:%d\n",FifoHead,FifoTail );
    //        printQueue(LruHead,MruHead);
        }
        else if(pageReplacementAlgo == LRU_CLOCK){ 
            globalPageTable[freePage].refBit = 1;
            //printLRU_CLOCK();
        }
//    (void) interrupt->SetLevel(oldLevel);
}

void
AddrSpace::DeletePageEntry(int deletedPage){
//    IntStatus oldLevel = interrupt->SetLevel(IntOff); 
    if(pageReplacementAlgo == FIFO){
        if(FifoHead == FifoTail){
            FifoTail = FifoHead = -1;
        }
        else if(deletedPage == FifoHead){
            FifoHead = globalPageTable[deletedPage].nextIndex;
            globalPageTable[FifoHead].prevIndex = -1;
        }
        else if(deletedPage == FifoTail){
            FifoTail = globalPageTable[deletedPage].prevIndex;
            globalPageTable[FifoTail].nextIndex = -1;
        }
        else{
            int next = globalPageTable[deletedPage].nextIndex;
            int prev = globalPageTable[deletedPage].prevIndex;
            globalPageTable[next].prevIndex = prev;
            globalPageTable[prev].nextIndex = next;
        }
    }
    else if(pageReplacementAlgo == LRU){
        if(LruHead == MruHead){
            LruHead = MruHead = -1;
        }
        else if(deletedPage == LruHead){
            LruHead = globalPageTable[deletedPage].nextIndex;
            globalPageTable[LruHead].prevIndex = -1;
        }
        else if(deletedPage == MruHead){
            MruHead = globalPageTable[deletedPage].prevIndex;
            globalPageTable[MruHead].nextIndex = -1;
        }                
        else{
            int next = globalPageTable[deletedPage].nextIndex;
            int prev = globalPageTable[deletedPage].prevIndex;
            globalPageTable[next].prevIndex = prev;
            globalPageTable[prev].nextIndex = next;
        }
    }
    else if(pageReplacementAlgo == LRU_CLOCK){
        globalPageTable[deletedPage].refBit = 0;
        //doing nothing as using indices to circulate         
    }
    globalPageTable[deletedPage].thread = NULL;
    globalPageTable[deletedPage].nextIndex = -1;
    globalPageTable[deletedPage].prevIndex = -1;
    globalPageTable[deletedPage].virtualIndex = -1;
    globalPageTable[deletedPage].refBit = 0;
//    (void) interrupt->SetLevel(oldLevel);
    return;
}
